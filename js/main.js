///Theory

// 1. Опишіть своїми словами що таке Document Object Model (DOM).
// Отображение html документа в виде дерева тегов.
// 2. Яка різниця між властивостями HTML-елементів innerHTML та innerText?
// InnerText выводит чисто значение тега включая внутренние теги, а innerHTML не выводит включая теги.
// 3. Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий?
// Без разницы как обращаться, главное правильно достучаться до него.
///Practice

let allParagraphElements = document.querySelectorAll("p");
console.log(allParagraphElements);
for (let paragraph of allParagraphElements) {
    paragraph.style.backgroundColor = "#ff0000";
}
let optionsListElement = document.getElementById("optionsList");
console.log(optionsListElement);
console.log(optionsListElement.parentElement);
console.log(optionsListElement.childNodes);
for (let element of optionsListElement.childNodes) {
    console.log(element.nodeType, element.nodeName);
}
let testParagraphElement = document.getElementById("testParagraph");
testParagraphElement.innerText = "This is a paragraph";
let mainHeaderElements = document.getElementsByClassName("main-header");
console.log(mainHeaderElements);
for (let mainHeader of mainHeaderElements) {
    mainHeader.classList.add("nav-item");
}
let sectionTitleElements = document.getElementsByClassName("section-title");
for (let sectionTitle of sectionTitleElements) {
    sectionTitle.classList.remove("section-title");
}



